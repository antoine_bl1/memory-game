import { Button, Drawer, makeStyles, Theme, useTheme } from "@material-ui/core";
import { CSSProperties } from "@material-ui/core/styles/withStyles";
import { CardsValues } from "./Game";

export interface DrawerMenuProps {
  displayed: boolean;
  cardsValues: Array<CardsValues>;
  currentCardsValuesTheme: string;
  onThemeChanged: (newTheme: string) => void;
  onClose: () => void;
}

const useStyles = makeStyles((theme: Theme) => ({
  container: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    overflow: "hidden",
  },
  iconBtn: {
    color: theme.palette.primary.main,
    height: "30vh",
    borderRadius: 0,
  },
}));

const DrawerMenu = ({
  displayed,
  cardsValues,
  currentCardsValuesTheme,
  onThemeChanged,
  onClose,
}: DrawerMenuProps) => {
  const classes = useStyles();
  const theme = useTheme();

  const handleThemeChanged = (themeClicked: string) => {
    if (themeClicked !== currentCardsValuesTheme) {
      onThemeChanged(themeClicked);
    }
    onClose();
  };

  const getBtnStyle = (cardsValuesTheme: string): CSSProperties => {
    const btnStyle: CSSProperties = {};
    const numberOfThemes = cardsValues.length;
    const btnWidth = 100 / numberOfThemes;
    btnStyle.width = `${btnWidth}vw`;
    btnStyle.fontSize = "10vh";
    if (cardsValuesTheme === currentCardsValuesTheme) {
      btnStyle.backgroundColor = theme.palette.secondary.main;
    }
    return btnStyle;
  };

  return (
    <Drawer anchor="bottom" open={displayed} onClose={onClose}>
      <div className={classes.container}>
        {cardsValues.map((cardsValue) => (
          <Button
            className={classes.iconBtn}
            style={getBtnStyle(cardsValue.theme)}
            onClick={() => handleThemeChanged(cardsValue.theme)}
          >
            {cardsValue.representativeValue}
          </Button>
        ))}
      </div>
    </Drawer>
  );
};

export default DrawerMenu;
