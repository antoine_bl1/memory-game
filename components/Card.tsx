import React from "react";
import classNames from "classnames";
import commonStyles from "../styles/Card.module.scss";
import { CSSProperties } from "@material-ui/core/styles/withStyles";

export interface CardProps {
  id: number;
  value: string;
  flipped: boolean;
  found: boolean;
  wrong: boolean;
  sideSize: number;
  beeingShuffled: boolean;
  onCardClicked: (id: number, value: string) => void;
}

const Card = ({
  id,
  value,
  flipped,
  found,
  wrong,
  sideSize,
  beeingShuffled,
  onCardClicked,
}: CardProps) => {
  // add css classes needed to flip, tag as found or wrong
  // more readable in a css file with complex attributes, transitions etc
  const getCardClasses = () => {
    const classes = [commonStyles.flipCard];
    if (flipped) {
      classes.push(commonStyles.flipped);
    } else {
      classes.push(commonStyles.clickableCard);
    }
    if (found) {
      classes.push(commonStyles.foundPair);
    }
    if (wrong) {
      classes.push(commonStyles.wrongPair);
    }
    if(beeingShuffled){
      classes.push(commonStyles.beeingShuffled);
    }
    return classes;
  };

  const getCardStyle = (): CSSProperties => {
    const gridStyle: CSSProperties = {
      width: `${sideSize}px`,
      height: `${sideSize}px`,
    };
    return gridStyle;
  };

  return (
    <div
      className={classNames(getCardClasses())}
      style={getCardStyle()}
      onClick={() => onCardClicked(id, value)}
    >
      <div className={commonStyles.flipCardInner}>
        <div className={commonStyles.flipCardFront}>
          <span className={commonStyles.content}>?</span>
        </div>
        <div className={commonStyles.flipCardBack}>{value}</div>
      </div>
    </div>
  );
};

export default Card;
