module.exports = {
  basePath: "/memory-game",
  assetPrefix: "/memory-game",
  publicRuntimeConfig: {
    // Will be available on client
    prefix: "/memory-game",
  },
};
