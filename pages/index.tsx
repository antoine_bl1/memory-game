import Game from "../components/Game";
import cardsValues from "../public/cards-values.json";

const Home = () => {
  return <Game cardsValues={cardsValues} />;
};

export default Home;
