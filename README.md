## Context

This application is a memory game.

This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000/memory-game](http://localhost:3000/memory-game) with your browser to see the result.

You can start editing the page by modifying `pages/index.tsx`. The page auto-updates as you edit the file.

## Deployment

The projet is automatically deployed on a static **gitlab page** when code is merged on **master** branch (configured by the **.gitlab-ci.yml**) : [https://antoine_blin.gitlab.io/memory-game](https://antoine_blin.gitlab.io/memory-game)
